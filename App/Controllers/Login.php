<?php
namespace App\Controllers;
use App\Core\Controller as Controller;
/**
 *  Login
 */
class Login extends Controller
{
	
	public function __construct()
	{
		parent::__construct();
	}

	public function indexAction()
	{
			$this->view->render('b0', 'login');
	}

	protected function before(){}

    protected function after(){}

	
	function run()
	{
		//echo $this;
		$this->model->run();
	}


} //END CLASS