<?php
namespace App\Controllers;
use App\Core\Controller as Controller;
/**
 *  About
 */
class About extends Controller
{

	public function __construct()
	{
		parent::__construct();
        
		//Setting global css file
		self::setStyles('app');
	}

	public function indexAction()
	{
		$this->view->render('f0', 'about');
	}
	
	protected function before(){}

    protected function after(){}


} //END CLASS