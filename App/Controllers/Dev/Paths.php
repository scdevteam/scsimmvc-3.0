<?php
namespace App\Controllers\Dev;
use App\Core\Controller as Controller;
/**
 *  Paths
 */
class Paths extends Controller
{
	
	public function __construct()
	{
		parent::__construct();
	}

	public function IndexAction()
	{
			$this->view->render('d0', 'paths');
	}

    protected function before(){}

    protected function after(){}

} //END CLASS